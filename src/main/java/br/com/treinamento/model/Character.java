package br.com.treinamento.model;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Character {

	private Integer id;
	private String name;
	private String description;
	private Calendar modified;
	
	
	@JsonIgnore
	private String code;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Calendar getModified() {
		return modified;
	}
	public void setModified(Calendar modified) {
		this.modified = modified;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
