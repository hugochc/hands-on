package br.com.treinamento.dojo.controller;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.model.Character;

@RestController
public class PesquisaController {

	private static final String URL = "https://gateway.marvel.com/v1/public/characters";
	private static final String TS = "1";
	private static final String PUBLIC_KEY = "fe55dcf23d091fa418698b9e27087fae";
	private static final String PRIVATE_KEY = "1f330ad1149fd40ce4f0fb0371a4e7440966db62";
	private static final String HASH = "0db4609f20df049594f8c555d0c52cc3";
	
	@RequestMapping(value = "/pesquisa", method = RequestMethod.GET)
	public ResponseEntity<List<Character>> pesquisa() throws JsonParseException, JsonMappingException, IOException {
		
		StringBuilder sb = new StringBuilder();
		sb.append(URL);
		sb.append("?ts=").append(TS);
		sb.append("&apikey=").append(PUBLIC_KEY);
		sb.append("&hash=").append(HASH);
		
		System.out.println(sb.toString());
		
		Client client = ClientBuilder.newClient();
		
		WebTarget target = client.target(sb.toString());
		
		Builder request = target.request();
		
		String response = request.get(String.class);
		
		System.out.println(response);
		
		List<Character> list = getList(response);
		
		return new ResponseEntity<List<Character>>(list, HttpStatus.OK);	
		
	}
	
	
	private List<Character> getList(String response) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<br.com.treinamento.model.Character> list = (List<Character>) mapper.readValue(response, br.com.treinamento.model.Character.class);
		return list;
	}
	
	
	
}
